package com.tr.samples.quickstart.repository;

import com.tr.samples.quickstart.domain.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mustafa Erbin on 22.08.2019
 */
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
