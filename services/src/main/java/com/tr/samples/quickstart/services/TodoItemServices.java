package com.tr.samples.quickstart.services;

import com.tr.nebula.persistence.jpa.services.JpaService;
import com.tr.samples.quickstart.domain.TodoItem;
import com.tr.samples.quickstart.repository.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Mustafa Erbin on 22.08.2019
 */
@Service
public class TodoItemServices extends JpaService<TodoItem, Long> {

    private TodoItemRepository repository;

    @Autowired
    public TodoItemServices(TodoItemRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public List<TodoItem> findByParentOid(Long parentOid) {
        return repository.findByParent_id(parentOid);
    }
}
