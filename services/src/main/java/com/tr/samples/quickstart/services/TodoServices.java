package com.tr.samples.quickstart.services;

import com.tr.nebula.persistence.jpa.services.JpaService;
import com.tr.samples.quickstart.domain.Todo;
import com.tr.samples.quickstart.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mustafa Erbin on 22.08.2019
 */
@Service
public class TodoServices extends JpaService<Todo, Long> {

    @Autowired
    public TodoServices(TodoRepository repository) {
        super(repository);
    }
}
